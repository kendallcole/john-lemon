﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoringSystem : MonoBehaviour
{

    public static int Score;
    public Text ScoreText;

    // Start is called before the first frame update
    void Start () {
        ScoreText.text= "Score: " + Score.ToString();

    }

}
