﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CapsuleCollect : MonoBehaviour
{
    public AudioSource collectSound;
    public int Score;
    public UnityEngine.UI.Text ScoreText;


private void OnTriggerEnter (Collider other)
    {
    collectSound.Play();
    Destroy(gameObject);
    AddScore();
    }

    void AddScore() {
        Score++;
        ScoreText.text = Score.ToString();
    }

}
